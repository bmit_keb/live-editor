vex.defaultOptions.className = 'vex-theme-os';

    // Login options - vex
    var options = {
        message: 'Enter your username:',
        showCloseButton: false,
        escapeButtonCloses: false,
        input: [
            '<input name="username" type="text" placeholder="Username" required />'
        ].join(''),
        buttons: [
            $.extend({}, vex.dialog.buttons.YES, { text: 'Login' })
        ],
        callback: (data) => {
            if (!data) {
                $.alert("You need a username to enter.");
                vex.dialog.open(options);
            } else {
                socket.emit("newUser", data.username);
                user = data.username;
            }
        }
    };
    // -- Login options - vex

    vex.dialog.open(options);