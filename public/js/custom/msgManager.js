$("#form").submit((e) => {
        e.preventDefault();
        if ($("#txtInput").val()) {
            socket.emit("newMSG", $("#txtInput").val());
        }
        sent = true;
    });

socket.on("writeMSG", (data) => {
        $("#chat").prepend("<li class='msg'><div class='user'>" + data.user + ":</div> <div class='message'>" + data.msg + "</div></li>");
        if (sent) {
            $("#txtInput").val("");
            sent = false;
        }
    });
	
	socket.on("newStatus", (status) => {
        $("#chat").prepend("<li class='status'>" + status + "</li>");
    });