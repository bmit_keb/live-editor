returnHighlighted = (text) => {
    result = "";
    grammar = Prism.languages.markup;
    language = "html";
    result = Prism.highlight(text, grammar, language);
    return result;
}

handleCode = () => {
	output = returnHighlighted($("#codeInput").val());
            $("#codeOutput").html(output);
			$("#chat").css("top", "100px");
            socket.emit("newCode", $("#codeInput").val());
			changeChatPos();
			setEqualSize();
}

changeChatPos = () => {
	mytop = $("#codeOutput").offset().top;
	myheight = $(".command-line").height();
	$("#chat").css("top", parseInt(mytop) + 30 + myheight);
}


setEqualSize = () => {
	initHeight = $(".code-toolbar").height();
	$("#codeInput").height(initHeight - 25);
}

$(document).ready( () => {

$('textarea').autoResize();

if ($("#codeInput").val() != "") {
	handleCode();
	
}
else {
	$("#codeInput").val("<!DOCTYPE html>\n<html>\n<head>\n<title>\nPage Title\n</title>\n</head>\n<body>\n\n\n\n\n</body>\n</html>");
	handleCode();
	}

	$("#codeInput").keyup(function(event) {
    if (event.which == 13 || event.which == 8) {
        handleCode();
    } 
});
	

$(document).on("input paste", "#codeInput", () => {
		handleCode();
});

    socket.on("updateCode", (code) => {
        var cursorStart = $("textarea").get(0).selectionStart;
		var cursorEnd = $("textarea").get(0).selectionEnd;
		$("#codeInput").val(code.toString());
		handleCode();
		$("textarea").get(0).setSelectionRange(cursorStart, cursorEnd);
    });
	

});

$("#codeInput").bind( "keydown.autocomplete", ( event ) => {
        if ( self.options.disabled || self.element.propAttr( "readOnly" ) ) {
            return;
        }

        suppressKeyPress = false;
        var keyCode = $.ui.keyCode;
        switch( event.keyCode ) {
            case keyCode.UP:
                self._move( "previous", event );
                // prevent moving cursor to beginning of text field in some browsers
                event.preventDefault();
                break;
            case keyCode.DOWN:
                self._move( "next", event );
                // prevent moving cursor to end of text field in some browsers
                event.preventDefault();
                break;
			default: 
			self.val(self.val() + keyCode);
			output = returnHighlighted($("#codeInput").val());
            $("#codeOutput").html(output);
            socket.emit("newCode", $("#codeInput").val());
			break;
		}
});