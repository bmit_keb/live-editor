//  Connect to Socket.io Server
var socket = io.connect(window.location.href);
//  Set jquery variable for bootstrap
jquery = $;
sent = false;
user = "";



$(document).ready(() => {
	
	$('textarea').autoResize();
	$("#preInput").addClass("language-html");
	$("#preInput").css("background-color", "transparent");
	$("#preInput").css("width", "100%");
	$("#preInput").css("caret-color", "red");
	$("#preInput").css("border", "none");
	$("#preInput").css("box-shadow", "none");
	
	$.getScript("/js/custom/extra.js");
	$.getScript("/js/custom/login.js");
	$.getScript("/js/custom/window.js");
	$.getScript("/js/custom/msgManager.js");
	$.getScript("/js/custom/codeManager.js");
	
	$(".code-toolbar").css("position", "absolute");
	
    
    socket.on("message", (message) => {
		switch(message) {
        case "user confirmed":
            $("#inputLabel").text("Write your message here " + user);
            socket.send("request current");
			break;
        case "not a user":
            vex.dialog.open(options);
			break;
        case "used name":
            $.alert("This name has already been taken. Please use a different name.");
            vex.dialog.open(options);
			break;
		case "long name":
			$.alert("Please use a shorter name.");
			vex.dialog.open(options);
			break;
        case "get current":
            socket.emit("newCode", $("#codeInput").val());
			break;
		}
    });

});