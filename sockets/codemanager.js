module.exports = (socket, session, users) => {

    
    session.on("newCode", (code) => {

        if(users[session.id] != undefined && code != undefined) {
            session.broadcast.to("authenticated").emit("updateCode", code);
        }
    })

	session.on("message", (msg) => {
		if (msg == "request current") {
			if (Object.values(users).length > 0) {
				user = getID(users, 0);
				session.broadcast.to(user).send("get current");
    }
	};
});
}


// https://stackoverflow.com/questions/6765864/javascript-get-first-and-only-property-name-of-object
function getID( obj, count )
{
    if( isNaN( count ) ) count = Infinity
    var keys = []
    for( var it in obj )
    {
        if( keys.length > count ) break;
        keys.push( it );
    }
    return keys;
}