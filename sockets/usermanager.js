module.exports = (socket, session, users) => {

    session.on("newUser", (data) => {
		if (data.length > 15) {
			session.send("long name");
			console.log("Too long");
			return;
		}
        if (Object.values(users).indexOf(data) > - 1) {
            session.send("used name");
			return;
        }
            users[session.id] = data;
            joinMsg = "Client [" + data + "]: joined";
            session.join("authenticated");
            socket.in("authenticated").emit("newStatus", joinMsg);
            session.send("user confirmed");
    });

    session.on("disconnect", () => {
        if (users[session.id] != undefined) {
            exitMsg = "Client [" + users[session.id] + "]: left";
            socket.in("authenticated").emit("newStatus", exitMsg);
            session.leave("authenticated");
            delete users[session.id];
        }
    });
};