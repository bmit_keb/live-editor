module.exports = (socket, session, users) => {
    session.on("newMSG", (msg) => {
        // Check if user exists
        if (users[session.id] && msg.trim()) {
            user = users[session.id];
            socket.emit("writeMSG", { "user": user, "msg": msg })
        }
    });
}