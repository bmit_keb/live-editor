const express = require("express");             // Express
const app = express();                          // Request Handler
const port = 3000;                              // Portnummer
const http = require("http").Server(app);       // Webserver instanziieren
const socket = require("socket.io")(http);      // Sitzungshandler

app.use("/", express.static(__dirname + "/public"));    // Ordner public freigeben

var socketFile = require(__dirname + "/sockets/main.js")(socket);   // Sitzungs-handler starten


http.listen(port, () => console.log("Listening on port " + port));  // Webserver starten